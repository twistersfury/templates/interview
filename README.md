## Interview Template

This is an empty php project for the purpose of quick prototyping.
It was originally created to be used during the interview process to handle "take home" work.

This project can double as a initial project for PHP 8.2 with Codeception.

## Installation

Run `make`. It should do the rest.