<?php

namespace Tests\Unit;

use Codeception\Test\Unit;
use TwistersFury\Interview\Dummy;

class DummyTest extends Unit
{
    /** @var \TwistersFury\Interview\Dummy */
    private $testSubject;

    /** @var \Tests\Support\UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Dummy();
    }

    public function testElse() {
        $this->assertEquals("else", $this->testSubject->doSomething());
    }
}